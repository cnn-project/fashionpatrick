import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:fashionpatrick/models/analyzedImage.dart';

class ImageAnalysisServices {
  final String apiUrl;
  Dio dio = new Dio();

  ImageAnalysisServices([this.apiUrl="http://35.228.249.118:8000"]);

  Future<AnalysedImage> postImage(File image) async {
    AnalysedImage analysedImage = new AnalysedImage();
    List<dynamic> correspondingImage = new List<dynamic>();

    // L'image est convertie en base 64 et stocké dans une liste de bytes
    List<int> imageBytes = image.readAsBytesSync();
    String base64Image = base64Encode(imageBytes);
    var responseJSON;

    // L'image est insérée dans le champ FormData de la requete
    FormData formData = new FormData.fromMap({
      "image":  "data:image/png;base64," +  base64Image
    });

    // Envoie de l'image vers le serveur
    await dio.post('$apiUrl/search/', data: formData)
        .then((response) => responseJSON = json.decode(json.encode(response.data)))
        .catchError((error) => print("Erreur récuperation api: $error"));

    // On recupère la liste d'images ressemblantes
    correspondingImage = await this.getCorrespondingImage(responseJSON['url'].toString());

    // Construction d'un objet 'image analysée'
    Map<String, dynamic> mapAnalysedImage = {
      'remoteUrl': responseJSON['url'].toString(),
      'base64Image': base64Image,
      'correspondingPictures': correspondingImage
    };
    analysedImage.fromMap(mapAnalysedImage);

    return analysedImage;
  }

  // Renvoie la liste des images correspondantes, appelée uniquement dans la méthode 'postImage'
   Future<List<dynamic>> getCorrespondingImage(String analyzedImageUrl) async {
    List<dynamic> correspondingList;

    await dio.get(analyzedImageUrl)
        .then((response) => correspondingList = json.decode(json.encode(response.data['images'])))
        .catchError((error) => print("Erreur récuperation api: $error"));

    return correspondingList;
  }

}