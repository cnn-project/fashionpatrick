// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'analyzedImage.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AnalysedImageAdapter extends TypeAdapter<AnalysedImage> {
  @override
  final typeId = 0;

  @override
  AnalysedImage read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AnalysedImage()
      ..date = fields[0] as DateTime
      ..remoteUrl = fields[1] as String
      ..base64Image = fields[2] as String
      ..correspondingPictures = (fields[3] as List)?.cast<dynamic>();
  }

  @override
  void write(BinaryWriter writer, AnalysedImage obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.date)
      ..writeByte(1)
      ..write(obj.remoteUrl)
      ..writeByte(2)
      ..write(obj.base64Image)
      ..writeByte(3)
      ..write(obj.correspondingPictures);
  }
}
