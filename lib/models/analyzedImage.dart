import 'package:hive/hive.dart';

part 'analyzedImage.g.dart';

@HiveType(typeId:0)
class AnalysedImage {
  @HiveField(0)
  DateTime date;

  @HiveField(1)
  String remoteUrl;

  @HiveField(2)
  String base64Image;

  @HiveField(3)
  List<dynamic> correspondingPictures;

  AnalysedImage();

  void fromMap(Map<String, dynamic> map){
    this.date = new DateTime.now();
    this.remoteUrl = map['remoteUrl'];
    this.base64Image = map['base64Image'];
    this.correspondingPictures = map['correspondingPictures'];
    }

  @override
  String toString() {
    return 'AnalysedImage{remoteUrl: $remoteUrl, createOn: $date}';
  }
}