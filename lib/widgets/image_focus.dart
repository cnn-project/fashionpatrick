import 'dart:convert';

import 'package:fashionpatrick/models/analyzedImage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class ImageFocus extends StatelessWidget {
  final AnalysedImage analysedImage;

  const ImageFocus({Key key, this.analysedImage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text(
                this.analysedImage.correspondingPictures[0].toString().split('/')[8].replaceAll('_', ' '),
                style: TextStyle(fontSize: 30)
            ),
            leading: IconButton(
              color: Colors.white,
              icon: Icon(Icons.arrow_back),
              onPressed: (){
                Navigator.of(context).pop();
              },
            ),
            expandedHeight: MediaQuery.of(context).size.height / 4,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              background: Hero(
                  tag: 'analyzedImage-${analysedImage.hashCode}',
                  child: Image.memory(base64Decode(analysedImage.base64Image), fit: BoxFit.cover)),
            ),
            floating: true,
            pinned: true,
            snap: true,
            elevation: 50,
            backgroundColor: Colors.pink,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                final String itemImageUrl = this.analysedImage.correspondingPictures[index];
                final double itemHeight = MediaQuery.of(context).size.height / 1.6;
                return Column(
                    children: <Widget> [
                      SizedBox(height: 10),
                      Container(
                          height: itemHeight,
                          width: itemHeight / 1.675,
                          child: _listItem(context, itemImageUrl)
                      )
                    ]
                );
              },
              childCount: this.analysedImage.correspondingPictures.length
            ),
          ),
        ],
      ),
    );
  }

  Widget _listItem(BuildContext context, String itemImageUrl) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15.0),
      child: Container(
        child: Image.network(itemImageUrl, fit: BoxFit.fitHeight),
      ),
    );
  }
}