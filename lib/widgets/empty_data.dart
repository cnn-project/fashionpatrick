import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';

class EmptyData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _starRow(),
        GradientText(
            "Voulez vous analyser une image ?",
            gradient: LinearGradient(colors: [Colors.deepPurple, Colors.deepOrange, Colors.pink]),
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 42,
                fontStyle: FontStyle.italic
            ),
        ),
        _starRow(),
      ],
    );
  }

  Widget _starRow(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(Icons.star, color: Colors.pink),
        Icon(Icons.star, color: Colors.pink),
        Icon(Icons.star, color: Colors.pink),
      ],
    );
  }
}
