import 'dart:io';

import 'package:fashionpatrick/models/analyzedImage.dart';
import 'package:fashionpatrick/widgets/empty_data.dart';
import 'package:fashionpatrick/widgets/image_card.dart';
import 'package:fashionpatrick/widgets/image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:image_picker/image_picker.dart';

class HomeController extends StatefulWidget {
  @override
  _HomeControllerState createState() => _HomeControllerState();
}

class _HomeControllerState extends State<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fashion Patrick"),
        backgroundColor: Colors.pink,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.do_not_disturb_on),
            onPressed: () {
              Hive.box('analyzedImages').clear();
              setState(() { });
            }
          )
        ],
      ),
      body: _scaffoldData(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.pinkAccent,
        child: Icon(Icons.add_photo_alternate),
        onPressed: () => _openCupertinoMenu(),
      )
    );
  }

  Widget _scaffoldData(){
    return Hive.box('analyzedImages').length > 0 ? _buildListView() : EmptyData();
  }

  _openCupertinoMenu(){
     return showCupertinoModalPopup(
       context: context,
       builder: (BuildContext context) => CupertinoActionSheet(
           title: const Text('Analyser une image'),
           actions: <Widget>[
             CupertinoActionSheetAction(
               child: const Text('Caméra'),
               onPressed: () {
                 Navigator.pop(context, 'Camera');
                 _getImage(true);
               },
             ),
             CupertinoActionSheetAction(
               child: const Text('Bibliothèque'),
               onPressed: () {
                 Navigator.pop(context, 'Libray');
                 _getImage(false);
               },
             )
           ],
           cancelButton: CupertinoActionSheetAction(
             child: const Text('Annuler'),
             isDefaultAction: true,
             onPressed: () {
               Navigator.pop(context, 'Cancel');
             },
           )),
     );
  }

  Future _getImage(isCamera) async {
    var image = await ImagePicker.pickImage(
        source: isCamera ? ImageSource.camera : ImageSource.gallery,
        maxHeight: 1000,
        maxWidth: 1000,
        imageQuality: 100
    );

    if (image != null){
      setState(() {
        uploadImageRouter(image);
      });
    }
  }

  ListView _buildListView() {
    final analyzedImageBox = Hive.box('analyzedImages');
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: analyzedImageBox.length,
      itemBuilder: (context, index) {
        final analyzedImage = analyzedImageBox.get(index) as AnalysedImage;
        return Column(
            children: <Widget> [
              SizedBox(height: 10),
              Container(
                  width: MediaQuery.of(context).size.width / 1.1,
                  child: ImageCard(analysedImage: analyzedImage),
              )
            ]
        );
      });
  }

  void uploadImageRouter(File image) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => AppImagePicker(image: image))
    ).then((_){setState(() { });});
  }

  void retourAcceuil(){
    Navigator.of(context).pop();
  }
}
