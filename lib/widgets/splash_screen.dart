import 'package:fashionpatrick/widgets/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_gradient_colors/flutter_gradient_colors.dart';

class PatrickSplashScreen extends StatefulWidget {
  @override
  _PatrickSplashScreenState createState() => new _PatrickSplashScreenState();
}

class _PatrickSplashScreenState extends State<PatrickSplashScreen> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 2,
        image: new Image.asset('assets/images/patrick.png', fit: BoxFit.fill,),
        gradientBackground: LinearGradient(
          colors: GradientColors.darkPink,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        photoSize: 100.0,
        onClick: () => print("Welcome on Patrick"),
        loaderColor: Colors.white,
      navigateAfterSeconds: new FutureBuilder(
        future: Hive.openBox('analyzedImages'),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.connectionState == ConnectionState.done) {
            if(snapshot.hasError){
              return Scaffold(body: Text(snapshot.error.toString()));
            } else {
              return HomeController();
            }
          } else {
            return Scaffold();
          }
        }
      )
    );
  }
}