import 'dart:convert';

import 'package:fashionpatrick/models/analyzedImage.dart';
import 'package:flutter/material.dart';
import 'image_focus.dart';

class ImageCard extends StatelessWidget {
  final AnalysedImage analysedImage;

  const ImageCard({Key key, this.analysedImage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'analyzedImage-${analysedImage.hashCode}',
      child: ClipRRect(
          borderRadius: BorderRadius.circular(15.0),
          child: GestureDetector(
            onTap: (){
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ImageFocus(analysedImage: analysedImage)));
            },
            child: Container(
              alignment: Alignment.topLeft,
              child: Image.memory(base64Decode(analysedImage.base64Image)),
            ),
          )
      ),
    );
  }
}
