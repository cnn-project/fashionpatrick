import 'dart:async';
import 'dart:io';
import 'package:fashionpatrick/models/analyzedImage.dart';
import 'package:fashionpatrick/services/imageAnalysisServices.dart';
import 'package:fashionpatrick/widgets/image_focus.dart';
import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:hive/hive.dart';
import 'package:image_cropper/image_cropper.dart';

class AppImagePicker extends StatefulWidget {
  final File image;

  const AppImagePicker({Key key, this.image}) : super(key: key);
  @override
  _AppImagePickerState createState() => _AppImagePickerState();
}

class _AppImagePickerState extends State<AppImagePicker> {
  File _image;
  bool _isUploading = false;
  ImageAnalysisServices imageApi = new ImageAnalysisServices();
  AnalysedImage analysedImage;

  @override
  void initState() {
    super.initState();
    this._image = widget.image;
  }

  @override
  Widget build(BuildContext context) {
    return _isUploading == false ? Scaffold(
        appBar: AppBar(
          title: Text("Patrick"),
          backgroundColor: Colors.pink,
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: (){ cropImage(); },
                child: _image != null ? Image.file(_image,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 2,
                  fit: BoxFit.cover,
                ) : (){ retourAcceuil(); },
              ),
              GradientText(
                  "Voulez vous analyser une image ?",
                  gradient: LinearGradient(colors: [Colors.deepPurple, Colors.deepOrange, Colors.pink]),
                  style: TextStyle(fontSize: 45),
                  textAlign: TextAlign.center
              ),
              SizedBox(height: 15),
              ButtonBar(
                alignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    child: const Text(
                        "OUI", style: TextStyle(fontSize: 30.0)),
                    onPressed: () async {
                      setState(() {
                        new Timer(const Duration(seconds: 1), () =>
                            setState(() {
                              _isUploading = true;
                            }));
                      });
                      analysedImage = await imageApi.postImage(this._image);
                      setState(() => this._isUploading = false);
                      Hive.box('analyzedImages').add(analysedImage);
                      retourAcceuil();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ImageFocus(analysedImage: analysedImage))
                      );
                    },
                    color: Colors.green,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    elevation: 5,
                    splashColor: Colors.purple,
                    padding: EdgeInsets.symmetric(
                        vertical: 16.0, horizontal: 24.0),
                  ),
                  RaisedButton(
                    child: const Text(
                        "NON", style: TextStyle(fontSize: 27.0)),
                    onPressed: () {
                      retourAcceuil();
                    },
                    color: Colors.red,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    elevation: 5,
                    splashColor: Colors.redAccent,
                    padding: EdgeInsets.symmetric(
                        vertical: 16.0, horizontal: 24.0),
                  ),
                ],
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
        )
    ) : Scaffold(
        body: Center(
            child: CircularProgressIndicator(backgroundColor: Colors.white)
        )
    );
  }

  Future cropImage() async {
    var image = await ImageCropper.cropImage(
        sourcePath: _image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        )
    );
    setState(() {
      if (image != null) {
        this._image = image;
      }
    });
  }

  void retourAcceuil(){
    Navigator.of(context).pop();
  }
}
