# Fashionpatrick - Fashion image recognition app

[Patrick](https://gitlab.com/cnn-project/patrick) but fashion 😎

Front end of an image recognition project made with Flutter.


This project has been carried out as part of my engineering training.

### Project description

##### French description 🇫🇷

> Le fonctionnement de l’application mobile et l’enchainement des écrans sont les suivants.
>
> Le premier écran est destiné au choix de l’image requête. Deux possibilités sont à offrir à l’utilisateur pour choisir cette image : soit l’utilisateur sélectionne une image existante dans la base du téléphone, soit l’utilisateur réalise une nouvelle photo. Dans les deux cas, un écran spécifique est à prévoir pour réaliser cette opération.
>
> Une fois l’image sélectionnée, l’application effectue la demande de recherche d’images similaires auprès du serveur selon les principes décrits dans la section 2
>
> Quand le traitement de recherche est terminé, le résultat obtenu (c-à-d les images de références les plus proches) est présenté à l’utilisateur dans un nouvel écran. Pour cet écran, il faut prévoir la présentation de plusieurs images.
>
> La figure suivante présente les différents écrans à réaliser pour l’application et leur enchaînement.


##### English description 🇬🇧

> The mobile application's operation and the sequence of screens are as follows.
>
> The first screen is for selecting the requested image. There are two possibilities for the user to choose this image: either the user selects an existing image in the phone base, or the user makes a new picture.
> In both cases, a specific screen is provided to perform this operation.
> Once the image has been selected, the application requests the server to search for similar images using our [API Plankton](https://gitlab.com/cnn-project/computer_vision).
>
> When search processing is complete, the result (i.e. the closest reference images) is presented to the user in a new screen. For this screen, several images are presented.
>
> The following figure shows the different screens to be realized for the application and their sequence.

### 🚀 Demo of the application 🚀

<div align="left">
    <img src="https://media.giphy.com/media/YMvrsMO1zfowaA4eHW/giphy.gif" width="35%">
</div>

### Run the project locally

Download or clone the project.

Run `flutter create .` and `flutter pub get` in order to install dependencies.

Run `flutter run` to launch the app on a emulator. The app will automatically reload if you change any of the source files.

:warning: This app has been designed only for mobile version of flutter (Android/iOS). :warning:

### Further help

To get more help don't hesitate to contact me.
